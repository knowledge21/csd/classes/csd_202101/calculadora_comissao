from unittest import TestCase
import sistema_vendas.calculadora_comissao as calculadora_comissao

#Lula
#Thiago
#Joao
#Moacir
#Bruno
#Cintia
#Luciano
#Caio

class teste_calculadora_comissao(TestCase):


    def teste_calculo_comissao_com_venda_de_1000_reais_retorna_comissao_de_50_reais(self):
        venda = 1000
        comissao_esperada = 50

        comissao = calculadora_comissao.calcular(venda)

        self.assertEqual(comissao_esperada,comissao)

    def teste_calculo_comissao_com_venda_de_11000_reais_retorna_comissao_de_660_reais(self):
        venda = 11000
        comissao_esperada = 660
        comissao = calculadora_comissao.calcular(venda)

        self.assertEqual(comissao_esperada,comissao)

    def teste_calculo_comissao_com_venda_de_500_reais_retorna_comissao_de_25_reais(self):
        venda = 500
        comissao_esperada = 25

        comissao = calculadora_comissao.calcular(venda)

        self.assertEqual(comissao_esperada,comissao)

    def teste_calculo_comissao_com_venda_de_20000_reais_retorna_comissao_de_1200_reais(self):
        venda = 20000
        comissao_esperada = 1200

        comissao = calculadora_comissao.calcular(venda)

        self.assertEqual(comissao_esperada,comissao)

    def teste_calculo_comissao_com_venda_de_55_59_reais_retorna_comissao_de_2_77_reais(self):
        venda = 55.59
        comissao_esperada = 2.77

        comissao = calculadora_comissao.calcular(venda)

        self.assertEqual(comissao_esperada,comissao)